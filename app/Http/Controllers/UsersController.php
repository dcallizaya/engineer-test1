<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;

class UsersController extends Controller
{
	public function index(Request $request, Generator $faker) {
		$list = [];
		for($i=0; $i < 100; $i++) {
			$list[]=[
		        "usr_username" => $faker->name,
		        "usr_email" => $faker->unique()->safeEmail,
		        "usr_create_date" => $faker->date,
		    ];
	    }
	    return ["data" => $list];
	}

	public function approve() {

	}
}
